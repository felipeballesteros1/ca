import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import Scroll from 'react-scroll-to-element';
import { Menu, Image } from 'semantic-ui-react';
import './style2.scss';
import logo from './../../../assets/img/logo.png';

export default class Nav extends Component {
  state = {
    activeItem: '',
  };

  render() {
    return (
      <div>
        <Menu text vertical className="main-menu-text">
          <Menu.Item className="image-item">
            <Image centered size="tiny" src={logo} />
          </Menu.Item>
          {/* <Menu.Item className="menu-item" name="group" active={this.state.activeItem === 'group'} onClick={this.handleItemClick}>
            <Scroll type="id" element="group" offset={-100}>
              <Link to="/"> Group </Link>
            </Scroll>
          </Menu.Item> */}
        </Menu>
      </div>
    );
  }
}

import React, { Component } from 'react';
// import PropTypes from 'prop-types';
import { Transition, Visibility } from 'semantic-ui-react';
import './style.scss';

export default class Visible extends Component {
  state = {
    visible: false,
  };

  onScreen = () => {
    this.setState({ visible: true });
  };

  offScreen = () => {
    this.setState({ visible: false });
  };

  render() {
    return (
      <Visibility once onOnScreen={this.onScreen} onOffScreen={this.offScreen}>
        <Transition visible={this.state.visible} animation={this.props.animation} duration={this.props.duration}>
          {this.props.children}
        </Transition>
      </Visibility>
    );
  }
}

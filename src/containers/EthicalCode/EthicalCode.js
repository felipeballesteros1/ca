import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Image, Container, Grid, Header } from 'semantic-ui-react';
import { Fade, Visible } from '../../components';
import './EthicalCode.scss';
import mainImg from './../../../assets/img/MAEthicalCode.png';

export default class EthicalCode extends Component {
  static propTypes = {
    prop: PropTypes,
  };

  render() {
    return (
      <div>
        <Fade className="transition-container">
          <div className="ethicalCodeContainer">
            <div className="image-container">
              <div id="ethicalcode" />
              <Image src={mainImg} fluid />
            </div>
            <br />
            <br />
            <br />
            <br />
            <br />
            <Container>
              <Visible animation="fade right" duration={1500}>
                <Header as="h3" textAlign="center" className="quote-header">
                  “We have dedicated our company to shape a more sustainable environment for luxury goods and services as a priority. “
                </Header>
              </Visible>
            </Container>
            <Container>
              <Grid columns={2}>
                <Grid.Column>
                  <Visible animation="fade up" duration={1500}>
                    <div>
                      <Header as="h3" textAlign="center">
                        Product Sourcing
                      </Header>
                      <Image src={mainImg} fluid />
                      <p className="content-one">
                        Minichiello Apparel is dedicated to conducting its operations on principles of ethical business practice and recognition of
                        the dignity of workers. We expect our business partners, our suppliers, and authorized sub-contractors to respect and adhere
                        to our standard guidelines and to all applicable national and local laws in the operation of their business. All suppliers are
                        required to conform to our vendor compliance guidelines.
                      </p>
                    </div>
                  </Visible>
                </Grid.Column>
                <Grid.Column>
                  <Visible animation="fade up" duration={1500}>
                    <div>
                      <Header as="h3" textAlign="center">
                        Employment
                      </Header>
                      <Image src={mainImg} fluid />
                      <p className="content-two">
                        Minichiello Apparel is committed to equality of opportunity in all its employment practices, policies and procedures. Within
                        the framework of the law, we are committed wherever feasible to achieving and maintaining a workforce which reflects the local
                        community in which we operate. Every possible step will be taken to ensure that individuals are treated fairly in all aspects
                        of their employment.
                      </p>
                    </div>
                  </Visible>
                </Grid.Column>
                <Grid.Column>
                  <Visible animation="fade up" duration={1500}>
                    <div>
                      <Header as="h3" textAlign="center">
                        Health &amp; Safety
                      </Header>
                      <Image src={mainImg} fluid />
                      <p className="content-three">
                        The Company is committed to ensure that the health, safety and welfare of its employees is protected. It will, so far as is
                        reasonably practicable, establish procedures and systems necessary to implement this commitment and to comply with its
                        statutory obligations on health and safety.
                      </p>
                    </div>
                  </Visible>
                </Grid.Column>
                <Grid.Column>
                  <Visible animation="fade up" duration={1500}>
                    <div>
                      <Header as="h3" textAlign="center">
                        Sustainability
                      </Header>
                      <Image src={mainImg} fluid />
                      <p className="content-four">
                        Minichiello Apparel continues a family spirit and legacy that focus on long-term vision. We challenge ourselves to high
                        standards of sustainability in our brands and our day to day operations.
                      </p>
                    </div>
                  </Visible>
                </Grid.Column>
              </Grid>
            </Container>
          </div>
          <br />
          <br />
          <br />
          <br />
          <br />
        </Fade>
      </div>
    );
  }
}

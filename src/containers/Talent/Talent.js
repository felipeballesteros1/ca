import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Image, Container, Grid, Header } from 'semantic-ui-react';
import { Fade, Visible } from '../../components';
import './Talent.scss';
import mainImg from './../../../assets/img/MATalent.png';

export default class Talent extends Component {
  static propTypes = {
    prop: PropTypes,
  };

  render() {
    return (
      <div>
        <Fade instant className="transition-container">
          <div className="talentContainer">
            <div className="image-container">
              <div id="talent" />
              <Image src={mainImg} fluid />
            </div>
          </div>
          <Container>
            <Header as="h1" textAlign="right">
              {' '}
              Do you want to join?{' '}
            </Header>
            <Grid columns={2}>
              <Grid.Column>
                <Visible animation="fade left" duration={1500}>
                  <div>
                    <p className="content-one">
                      At Minichiello Apparel, we aim to empower our employees to fulfil their potential and creativity. Founded and operated by
                      entrepreneurs, we encourage risk-takers and innovators. We know the value of a long-term vision: some of the most brilliant
                      ideas need time to develop and mature.
                    </p>
                    <p className="content-four">
                      If your ideal workplace is an environment where passion can flourish and people can thrive in the pursuit of outstanding
                      performance, perhaps Minichiello Apparel is the place for you.
                    </p>
                  </div>
                </Visible>
              </Grid.Column>
              <Grid.Column>
                <Visible animation="fade left" duration={1500}>
                  <div>
                    <Image className="image-blue" src={mainImg} fluid />
                    <p className="content-blue">
                      A person who is driven to succeed. A self-starter who are innovative, not afraid to learn, and with an attitude to do things
                      differently. A curious, hands-on and collaborative individual, ready to take on responsibility from day one. Does that sound
                      like you?
                    </p>
                  </div>
                </Visible>
              </Grid.Column>
            </Grid>
          </Container>
          <br />
          <br />
          <br />
          <br />
          <Container className="content-container">
            <Visible animation="fade up" duration={1500}>
              <Header as="h3" className="quote-header-small" textAlign="center">
                Contact us to learn more about the values, the culture and commitments that set us apart.
              </Header>
            </Visible>
          </Container>
        </Fade>
      </div>
    );
  }
}

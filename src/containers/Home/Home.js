import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Image, Container, Grid, Header, List } from 'semantic-ui-react';
import { Anchored } from 'react-scrollto-animated';
import { Nav, Fade, Visible, Nav2 } from '../../components';
import './Home.scss';
import mainImg from './../../../assets/img/MAGroup.png';
import picture from './../../../assets/img/picture.png';
import { Group, Talent, Technology, Brands, EthicalCode, InvestorRelations } from '../index.js';

export default class Home extends Component {
  render() {
    return (
      <div>
        {/* <Nav location={this.props.history.location.pathname} /> */}
        <Nav2 />
        <Group />
        {/* <Talent /> */}
        {/* <Technology /> */}
        {/* <Brands /> */}
        {/* <EthicalCode /> */}
        {/* <InvestorRelations /> */}
      </div>
    );
  }
}

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Image, Container, Grid, Header } from 'semantic-ui-react';
import { Fade, Visible } from '../../components';
import './Technology.scss';
import mainImg from './../../../assets/img/MATechnology.png';

export default class Technology extends Component {
  static propTypes = {
    prop: PropTypes,
  };

  render() {
    return (
      <div>
        <Fade instant className="transition-container">
          <div className="technologyContainer">
            <div className="image-container">
              <div id="technology" />
              <Image src={mainImg} fluid />
            </div>
            <Container>
              <Header as="h1">DINO SYSTEM</Header>
              <Grid columns={2}>
                <Grid.Column>
                  <Visible animation="fade right" duration={1500}>
                    <div>
                      <Image src={mainImg} fluid />
                    </div>
                  </Visible>
                </Grid.Column>
                <Grid.Column>
                  <Visible animation="fade left" duration={1500}>
                    <div>
                      <p className="content-three">
                        A proprietary artificial intelligence system that is being developed and designed in-house. The revolutionary DINO Expert
                        Design System is an expert styling system which incorporates fashion psychology into garment and style selections, and garment
                        design.
                      </p>
                    </div>
                  </Visible>
                </Grid.Column>
              </Grid>
            </Container>
            <Container>
              <Header as="h1">BIOMETRIC</Header>
              <Grid columns={2}>
                <Grid.Column>
                  <Visible animation="fade right" duration={1500}>
                    <div>
                      <p className="content-two">
                        Our biometric application makes it easy to predict accurate and detailed body measurements by utilizing one of the largest
                        cumulative data sets of human body scans in the world. The data- centered approach effectively eliminates the human error of
                        self measurements and the hassle of scanning or selfies.
                      </p>
                    </div>
                  </Visible>
                </Grid.Column>
                <Grid.Column>
                  <Visible animation="fade left" duration={1500}>
                    <div>
                      <Image src={mainImg} fluid />
                    </div>
                  </Visible>
                </Grid.Column>
              </Grid>
            </Container>
          </div>
        </Fade>
      </div>
    );
  }
}

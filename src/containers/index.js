import Group from './Group/Group';
import Talent from './Talent/Talent';
import Technology from './Technology/Technology';
import Brands from './Brands/Brands';
import EthicalCode from './EthicalCode/EthicalCode';
import InvestorRelations from './InvestorRelations/InvestorRelations';
import Home from './Home/Home';

export { Group, Talent, Technology, Brands, EthicalCode, InvestorRelations, Home };

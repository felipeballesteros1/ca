import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Image, Container, Header } from 'semantic-ui-react';
import { Fade, Visible } from '../../components';
import './InvestorRelations.scss';
import mainImg from './../../../assets/img/MAInvestmentRelations.png';

export default class InvestorRelations extends Component {
  static propTypes = {
    prop: PropTypes,
  };

  render() {
    return (
      <div>
        <Fade instant className="transition-container">
          <div className="investorRelationsContainer">
            <div  id="investorrelations" className="image-container">
              {/* <div id="investorrelations" /> */}
              <Image src={mainImg} fluid />
            </div>
            <Container>
              <Visible animation="fade right" duration={1500}>
                <Header as="h3" className="quote-header-small content-four">
                  The Company&#39;s financial information, press releases and other filings with the securities regulators are available under the
                  Company&#39;s available under the Company&#39;s profile at <a href="www.sedar.com">www.sedar.com</a>.
                </Header>
              </Visible>
            </Container>
          </div>
          <br />
          <br />
          <br />
          <br />
          <br />
          <br />
        </Fade>
      </div>
    );
  }
}

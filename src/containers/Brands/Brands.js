import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Image, Container, Header, Button } from 'semantic-ui-react';
import { Fade, Visible } from '../../components';
import './Brands.scss';
import mainImg from './../../../assets/img/MABrands.png';

export default class Brands extends Component {
  static propTypes = {
    prop: PropTypes,
  };

  render() {
    return (
      <div>
        <Fade instant className="transition-container">
          <div className="brandsContainer">
            <div className="image-container">
              <div id="brands" />
              <Image src={mainImg} fluid />
            </div>
            <Container>
              <Visible animation="fade in" duration={1500}>
                <Header as="h1" textAlign="left">
                  Brands that combine craftsmanship and creativity with technology.
                </Header>
              </Visible>
              <br />
              <br />
              <Visible animation="fade right" duration={1500}>
                <p>
                  Minichiello Apparel empowers its luxury lifestyle brands to reach their potential in the most imaginative way. Preserving an
                  identity and roots, while at the same time constantly reinventing themselves in order to appeal to their contemporaries, is the
                  “raison d’être” of the Brands under Minichiello Apparel.
                </p>
              </Visible>
            </Container>
            <Container>
              <Header as="h1" textAlign="center">
                Cattivo Ragazzo
              </Header>
              <Visible animation="fade in" duration={1500}>
                <div>
                  <Image src={mainImg} fluid />
                  <p>
                    Introducing our first brand, Cattivo Ragazzzo. Translated from Italian, a ‘cattivo ragazzo’ is the proverbial bad boy and our
                    client epitomizes this idealism but with a modern perspective. Our clients are edgy, bold, and gentlemanly, which is precisely how
                    today’s Cattivo Ragazzo lives their lives.  Cattivo Ragazzo is an advanced luxury brand of bespoke men’s wear in today’s digital
                    world
                  </p>
                  <Button floated="right">Go to Site.</Button>
                </div>
              </Visible>
            </Container>
            <br />
            <br />
            <br />
            <br />
          </div>
        </Fade>
      </div>
    );
  }
}

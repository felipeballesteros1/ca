import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Carousel from 'nuka-carousel';
import { Image, Container, Grid, Header, List, Icon } from 'semantic-ui-react';
import { Fade, Visible } from '../../components';
import './Group.scss';
import mainImg from './../../../assets/img/MAGroup.png';
import picture from './../../../assets/img/picture.png';

export default class Group extends Component {
  static propTypes = {
    prop: PropTypes,
  };

  render() {
    return (
      <div>
        <Fade className="transition-container">
          <div className="groupContainer">
            <div className="image-container">
              <div id="group" />
              <Image centered src={mainImg} />
            </div>
            {/* <Carousel autoplay > */}
            <Container className="square-container">
              <Header textAlign="center" as="h1">
                WE ARE MINICHIELLO.
              </Header>
              <div className="line-div" />
              <Grid columns={1}>
                <Grid.Column className="content-one-container">
                  <div>
                    <p className="content-desc">
                      The First of its kind, Minichiello Apparel, Inc is a global luxury lifestyle company dedicated to merging fashion and
                      technology, and protecting individuality of fashion. Minichiello Apparel is pioneering a new fashion segment, while championing
                      fashion as a force for good within our communities and society as a whole. We are building a data rich environment to host the
                      most exciting and innovative brands, products and services from around the world. All of whom are creating clothing and
                      lifestyle experiences with the upmost quality and care in mind. From local artisans, emerging designers and entrepreneurs to the
                      world most coveted luxury brands.
                    </p>
                    <p className="content-desc">
                      Based in Vancouver, British Columbia, Canada, the Company’s objective is managing development and operation of each of its
                      Brands. While respecting their identities and their autonomy, the Company will provide the innovative proprietary technology and
                      resources needed to create, produce and market their products and services through carefully selected channels.
                    </p>
                  </div>
                </Grid.Column>
              </Grid>
              <div className="line-div" />
            </Container>
            <Container className="filler" />
            <div className="image-container">
              <Image className="quote-image" centered src={mainImg} />
              {/* <Visible animation="fade right" duration={1500}> */}
              <div>
                <p className="quote-over-image">
                  “By creating and embedding cutting-edge technology into our core business mindset, we aim to disrupt the luxury lifestyle industry
                  with the highest-quality offerings while creating ultimate value for our clients, partners, and investors.”
                </p>
                <p className="over-image">-Dino</p>
              </div>
              {/* </Visible> */}
            </div>
            <Container>
              <Header as="h1" textAlign="center">
                {' '}
                OUR STORY{' '}
              </Header>
              <div className="line-div" />
              <Grid columns={1}>
                <Grid.Column>
                  {/* <Visible animation="fade left" duration={1500}> */}
                  <p className="content-desc">
                    <b>The Minichiello Legacy</b> began in Campo Basso, Italy - tailoring and its tools are historically woven into the very fabric of
                    this city. At the age of six, Paul Minichiello commenced in the apprenticeship and by age 12, he presented his father with a
                    bespoke three-piece suit.
                  </p>
                  {/* </Visible> */}
                </Grid.Column>
                <Grid.Column>
                  {/* <Visible animation="fade left" duration={1500}> */}
                  <p className="content-desc">
                    <b>Continuing</b> with his father’s passion for fashion, Dino, founder of Minichiello Apparel, launched a number of designer
                    labels and fashion ventures, including Minichiello retail, in the 90’s, and was recognised with a nomination for Best Emerging
                    Designer by the British Columbia Apparel industry.
                  </p>
                  {/* </Visible> */}
                </Grid.Column>
              </Grid>
              <div className="line-div" />
            </Container>
            <br />
            <br />
            <div className="image-container">
              <Image className="quote-image" centered src={mainImg} />
              {/* <Visible animation="fade right" duration={1500}> */}
              <div>
                <p className="quote-over-image">
                  “During my one last moment together, I had promised my dad that I would return to the families tailoring roots and continue my dad’s
                  legacy that he created.”
                </p>
                <p className="over-image">-Dino</p>
              </div>
              {/* </Visible> */}
            </div>
            <Container>
              <Header as="h1" textAlign="center">
                {' '}
                OUR STRATEGIES{' '}
              </Header>
              <div className="line-div" />
              <Grid columns={1}>
                <Grid.Column>
                  {/* <Visible animation="fade right" duration={1500}> */}
                  <div>
                    <p className="content-desc padded">
                      As a hybrid of fashion and technology, Minichiello Apparel’s brands use innovative and cutting-edge technology to:
                    </p>
                  </div>
                  {/* </Visible> */}
                </Grid.Column>
              </Grid>
              <Grid columns={3}>
                <Grid.Column>
                  <Header as="h2" textAlign="center">
                    <Icon name="user circle outline" />
                    Provide
                  </Header>
                  <p className="content-desc">quality made personalized product.</p>
                </Grid.Column>
                <Grid.Column>
                  <Header as="h2" textAlign="center">
                    <Icon name="pencil" />
                    Create
                  </Header>
                  <p className="content-desc">a data-driven, seamless, and fluid customer experience.</p>
                </Grid.Column>
                <Grid.Column>
                  <Header as="h2" textAlign="center">
                    <Icon name="line chart" />
                    Refine
                  </Header>
                  <p className="content-desc">
                    the supply chain and logistical process that will increase efficiency, both operationally and financially.
                  </p>
                </Grid.Column>
              </Grid>
            </Container>
              <div className="empty-layer">
            <Container>
                <Header as="h1" textAlign="center">
                  {' '}
                  OUR MANIFESTO{' '}
                </Header>
                <div className="line-div" />
                <Grid columns={2}>
                  <Grid.Column>
                    <p className="content-desc">
                      We cultivate perpetual creativity and innovation in our people and our brands. A strong commitment to quality, professional
                      development and continuous innovation is a hallmark of our company culture.
                    </p>
                  </Grid.Column>
                  <Grid.Column>
                    <p className="content-desc">
                      We collaborate for the good of our clients, employees, partners and clients. We create innovative ideas to safeguard our rich
                      legacy and empower future generations. <b>In other words, F*ck Fast Fashion.</b>
                    </p>
                  </Grid.Column>
                </Grid>
                <br />
                <br />
                <br />
                <br />
            </Container>
              </div>
            <Container>
              <Grid columns={1}>
                <Grid.Column>
                  <Header as="h1" textAlign="center">
                    OUR MANAGEMENT AND DIRECTORS
                  </Header>
                  <div className="line-div" />

                  {/* <Visible animation="fade up" duration={1500}> */}
                  <p className="content-desc">
                    Our team of management and directors is committed to transparent and sustainable business practices. We have dedicated ourselves
                    to build our company based on mutual trust, respect, fairness, and honesty with our clients, employees, partners and investors.
                  </p>
                </Grid.Column>
              </Grid>
              <br />
              <br />
              <Grid columns={4}>
                <Grid.Column>
                  <Image src={mainImg} fluid />
                </Grid.Column>
                <Grid.Column>
                  <Image src={mainImg} fluid />
                </Grid.Column>
                <Grid.Column>
                  <Image src={mainImg} fluid />
                </Grid.Column>
                <Grid.Column>
                  <Image src={mainImg} fluid />
                </Grid.Column>
                <Grid.Column>
                  <Image src={mainImg} fluid />
                </Grid.Column>
                <Grid.Column>
                  <Image src={mainImg} fluid />
                </Grid.Column>
                <Grid.Column>
                  <Image src={mainImg} fluid />
                </Grid.Column>
                <Grid.Column>
                  <Image src={mainImg} fluid />
                </Grid.Column>
              </Grid>
            </Container>
            {/* </Carousel> */}
            <br />
            <br />
          </div>
        </Fade>
      </div>
    );
  }
}

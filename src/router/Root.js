import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { Provider } from 'react-redux';
import { hot } from 'react-hot-loader';
import TransitionGroup from 'react-transition-group/TransitionGroup';
import store from '../redux/store';

import { Group, Talent, Technology, Brands, EthicalCode, InvestorRelations, Home } from '../containers';

class Root extends Component {
  render() {
    console.log(store);
    return (
      <Provider store={store}>
        <Router>
          <div className="page">
            <TransitionGroup className="main">
              <Route exact path="/" component={Home} />
            </TransitionGroup>
          </div>
        </Router>
      </Provider>
    );
  }
}

export default hot(module)(Root);
